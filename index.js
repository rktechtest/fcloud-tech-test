var express = require('express');
var app = express();
var path = require('path');
var s3 = require('./controllers/s3.js');
var mongoStore = require('./controllers/mongo')
var mongoose = require('mongoose');

mongoose.connect('mongodb://34.251.216.246:27017/fcloud', { useNewUrlParser: true });
app.use(express.static('public'))

app.get("/", function(req, res) {
    res.sendFile(path.join(__dirname + '/public/html', 'index.html'))
});

// Returns all ReturnedItem object from the database to display on front end
app.get("/getReturnedItems", function(req, res) {
    mongoStore.getReturnedItems().then(function(data) {
        res.status(200).send(data);
    }).catch(function(error) {
        res.status(500).send(error);
    });
});

// S3 Route for listing, parsing and deleting xml files in S3 bucket
app.use('/s3', s3);

app.listen(3000, "localhost");

module.exports = app