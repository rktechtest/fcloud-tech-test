$(function() {
    $.ajax({
        url: "http://localhost:3000/getReturnedItems",
        success: function(result) {

            if (result.length == 0) {
                $("#dataError").append("No items in database!");
            }

            var items = $("<ul>");

            $.each(result, function(i, item) {
                items.append($('<li>')
                    .append("Reference: " + item.ref)
                    .append('<br>')
                    .append("Return Description: " + item.returnDescription)
                    .append('<br>')
                    .append("Currency: " + item.currency)
                    .append('<br>')
                    .append("Return Code: " + item.returnCode))
            });

            $("#returnedItems").append(items);
        },
        error: function(error) {
            $("#dataError").append(error);
        }
    });
})