var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ReturnedDebitSchema = new Schema({
    ref: String,
    transCode: String,
    returnCode: String,
    returnDescription: String,
    originalProcessingDate: { type: Date, default: Date.now },
    valueOf: Boolean,
    currency: String,
    // Store reference to payer object attached to each returned item.
    playerAccount: {type: Schema.Types.ObjectId},
});

module.exports = mongoose.model('ReturnedDebit', ReturnedDebitSchema);