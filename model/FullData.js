var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// No properties added since we just insert the full document.
var FullDataSchema = new Schema({
});

module.exports = mongoose.model('FullDataSchema', FullDataSchema);