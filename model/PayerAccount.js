var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PayerAccountSchmea = new Schema({
    number: String,
    ref: String,
    name: String,
    sortCode: String,
    bankName: String,
    branchName: String
});

module.exports = mongoose.model('PayerAccount', PayerAccountSchmea);