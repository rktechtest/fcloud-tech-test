# fcloud-tech-test

# Running locally
## In project root, run 'npm install' to install dependencies
## In project root, run 'node index.js'
## Server should now be running on localhost:3000 - accessing this in browser should direct to basic UI

# API
## GET /getReturnedItems - triggered when loading UI webpage, returns all ReturnedItem objects stored in MongoDB.
## POST /s3/processXml - use this to parse files stored in S3 bucket and store in mongo. No body required in request. 

# Testing on AWS
## The app is also deployed on an EC2 instance and is accessible at: 
- http://ec2-34-251-216-246.eu-west-1.compute.amazonaws.com:3000/

#### There is a trigger set up on the S3 bucket that calls a Lambda function when a new XML file is added to the bank-xml/ bucket. The provided IAM account should have access to this bucket, therefore files can be placed in there for testing. Once the Lambda is triggered the file will be processed and afterwards placed in the /bank-aml/archive directory of the bucket.

# MongoDB Collections

## returneddebititems
### Sample query for data in project xml directory:
### db.returneddebits.find({"ref" : "X01234"}).pretty();

## payeraccounts
### Sample query for data in project xml directory:
### db.payeraccounts.find({"name" : "STEVE ROGERS"}).pretty();

## fulldataschemas
### Sample query for data in project xml directory:
### db.returneddebits.find().pretty();