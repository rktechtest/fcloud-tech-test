const nodemailer = require('nodemailer');

// Handler for sending emails, goes without saying with more time passwords here would not be in plain text!
function sendEmailNotfication() {
    var mainTransporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'techtestawsalerts@gmail.com',
            pass: 'testAlerts56'
        }
    });

    var mailOptions = {
        from: 'techtestawsalerts@gmail.com',
        to: 'techtestawsalerts@gmail.com',
        subject: 'New Data has been stored',
        text: 'New data has successfully been extracted and stored, you can view it here: http://localhost:3000/'
    }

    mainTransporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
}

module.exports = { sendEmailNotfication }