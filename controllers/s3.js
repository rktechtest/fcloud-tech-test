const AWS = require('aws-sdk');
const xmlToJson = require('./xmlToJson');
const extractor = require('./dataExtractor');
const express = require('express');
const router = express.Router();
const async = require('async');
const emailService = require('./emailService');

// Credentials for S3 bucket
AWS.config.update({
    accessKeyId: "AKIA2ZJNUB6OANUN556Q",
    secretAccessKey: "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"
});

let s3 = new AWS.S3()

router.post('/processXml', function(req, res) {

    let params = { Bucket: 'bank-xml', Delimiter: '/' }
    let fileKeys = [];

    // See what files are available
    s3.listObjects(params, function(err, data) {
        // Check there are files in the bucket
        if (data.Contents.length == 0) {
            res.send("No data");
        } else {
            async.each(data.Contents, function(file) {
                if (file.Size > 0) {
                    // Keep track of the file keys we process so we can archive later
                    fileKeys.push(file.Key);

                    // Params to retrieve object from bucket using getObject()
                    let objParams = { Bucket: 'bank-xml', Key: file.Key }
                    var obj = s3.getObject(objParams).promise();

                    obj.then(function(data, err) {
                        if (err)
                            console.log(err);

                        // Create a buffer from the retrieved object
                        let newBuff = Buffer.from(data.Body, 'utf-8');
                        // Parse the buffer taken from the xml file to json
                        let json = xmlToJson.xmlToJsonString(newBuff);

                        // Store and extract the data we need
                        extractor.extractAndSave(json).then(function(data) {
                            console.log("Success " + data)
                        }).catch(function(error) {
                            res.send(error)
                        });
                    });
                }
            });
            archiveObjects(fileKeys);
            emailService.sendEmailNotfication(fileKeys);
            res.send("Data Extraction Succesful");
        }
    })

    function archiveObjects(files) {

        async.each(files, function(file) {
            var copyParams = {
                Bucket: 'bank-xml',
                CopySource: '/bank-xml' + '/' + file,
                Key: file.replace(file, 'archive/' + file)
            };

            // Copy object from root bucket level to the archive folder
            s3.copyObject(copyParams).promise().then(function(data) {
                // Deletes the object from the root bucket level once they've been 
                var deleteParams = {
                    Bucket: 'bank-xml',
                    Key: file
                }

                s3.deleteObject(deleteParams, function(delErr, delData) {
                    if (delErr) {
                        console.log(delErr);
                    } else {
                        console.log('Deleted: ', file);
                    }
                });
            }).catch(function(err) {
                console.log(err);
            });
        });
    }
})

module.exports = router;