// const mongoose = require('mongoose');
const FullData = require('../model/FullData');
const ReturnedDebit = require('../model/ReturnedDebit');
const PayerAccount = require('../model/PayerAccount');
// mongoose.connect('mongodb://34.251.216.246:27017/fcloud', { useNewUrlParser: true });

// Store full parsed xml document
storeFullDocument = (json) => {
    FullData.collection.insertOne(json).then(function(data) {
        console.log("Full Document stored successfully");
    }).catch(function(error) {
        console.log(error);
    });
}

// Store all returned debit items
storeReturnedDebit = (returnedDebitItems) => {
    return new Promise((resolve, reject) => {
        ReturnedDebit.collection.insertMany(returnedDebitItems).then(function(data) {
            resolve("Returned Items Stored Successfully");
        }).catch(function(error) {
            reject(error);
        });
    });
}

// Store payer account details
storePayerAccount = (payerAccountItems) => {
    return new Promise((resolve, reject) => {
        PayerAccount.collection.insertMany(payerAccountItems).then(function(data) {
            resolve(data);
        }).catch(function(error) {
            reject(error);
        });
    });
}

// Return all returnedItems we have stored to display on UI
getReturnedItems = () => {
    return new Promise((resolve, reject) => {
        ReturnedDebit.find().then(function(data) {
            resolve(data);
        }).catch(function(error) {
            reject(error);
        });
    });
}

module.exports = { storeReturnedDebit, storeFullDocument, storePayerAccount, getReturnedItems }