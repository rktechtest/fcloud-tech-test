const xml2js = require('xml2js');
const parser = new xml2js.Parser({ trim: true, attrkey: "key", normalize: true, mergeAttrs: true, explicitArray: false });

// Function for converting the xml data to json using xml2js module.
// Pass in the data we want to parse and a callback function to return the result to the jsonString var
function xmlToJsonString(data) {

    let jsonString;
    parser.parseString(data, function(err, result) {
        jsonString = JSON.stringify(result);
    });

    return jsonString;
}

module.exports = { xmlToJsonString }