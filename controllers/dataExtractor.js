const ReturnedDebitItem = require('../model/ReturnedDebit');
const PayerAccount = require('../model/PayerAccount');
const mongoStore = require('./mongo');
const async = require('async');

function extractAndSave(result) {

    // Convert our json string to object
    let data = JSON.parse(result);

    // Store full document
    mongoStore.storeFullDocument(data)

    // Data we need to store is nested, chain down to get to the array
    let returnedItemArray = data.BACSDocument.Data.ARUDD.Advice.OriginatingAccountRecords.OriginatingAccountRecord.ReturnedDebitItem;

    // Keep an array of the extracted items so we can insertMany at the db level
    let returnedDebitItems = [];
    let payerAccountItems = [];


    async.each(returnedItemArray, function(item) {

        // Create a PayerAccount instance for each payer within a ReturnedDebitItem
        let payer = new PayerAccount({
            "number": item.PayerAccount.number,
            "ref": item.PayerAccount.ref,
            "name": item.PayerAccount.name,
            "sortCode": item.PayerAccount.sortCode,
            "bankName": item.PayerAccount.bankName,
            "branchName": item.PayerAccount.branchName
        });
        payerAccountItems.push(payer);

        // Create a ReturnedDebitItem instance for each ReturnedDebitItem
        let returnedDebitItem = new ReturnedDebitItem({
            "ref": item.ref,
            "transCode": item.transcOde,
            "returnCode": item.returnCode,
            "returnDescription": item.returnDescription,
            "originalProcessingDate": item.originalProcessingDate,
            "valueOf": item.valueOf,
            "currency": item.currency,
            "playerAccount": payer._id,
        });
        returnedDebitItems.push(returnedDebitItem);
    })

    // Store the array of ReturnedDebitItems and PayerAccount items
    let item = new Promise((resolve, reject) => {
        mongoStore.storeReturnedDebit(returnedDebitItems)
            .then(mongoStore.storePayerAccount(payerAccountItems).then(
                resolve("All items stored succesfully")
            ).catch(function(error) { reject(error) }));
    });

    // Return the items since we can display
    return item;
}

module.exports = { extractAndSave }