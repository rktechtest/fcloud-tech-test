let xmlToJson = require('../controllers/xmlToJson');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../index');
let should = chai.should();
let fs = require('fs');
let expect = chai.expect;

chai.use(chaiHttp);

/*
 * Test the /getReturnedItems  route
 */
describe('/GET ReturnedItems', () => {
    it('it should GET all the returned items', (done) => {
        chai.request(server)
            .get('/getReturnedItems')
            .end((err, res) => {

                if (err)
                    done(err);

                res.should.have.status(200);
                res.body.should.be.a('array');
                done();
            });
    });
});

// Test file we put on S3 before conversion
describe('XML before conversion', function() {
    it('should return false since it has not been converted to json', function() {

        var xml = fs.readFileSync(__dirname + '/testData/sample-2.xml', 'utf8');
        var isJSON = testJSON(xml);

        expect(isJSON).to.be.false;
    });
});

// Test XML file is converted to JSON after running through xmlToJson parser
describe('XML after conversion', function() {
    it('should return true since it has not been converted to json', function() {

        var xml = fs.readFileSync(__dirname + '/testData/sample-2.xml', 'utf8');
        var json = xmlToJson.xmlToJsonString(xml);
        var isJSON = testJSON(json);

        expect(isJSON).to.be.true;
    });
});

// Helper function to check if parsed document is JSON
function testJSON(text) {
    if (typeof text !== "string") {
        return false;
    }
    try {
        JSON.parse(text);
        return true;
    } catch (error) {
        return false;
    }
}